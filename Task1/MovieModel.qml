import QtQuick 2.0

ListModel{
    ListElement{
        name: "Ant Man"
        year: "2017"
        location: "Assets/images/poster_antMan.jpg"
        score: "★★★★☆"
    }
    ListElement{
        name: "Demolition"
        year: "2016"
        location: "Assets/images/poster_demolition.jpg"
        score: "★★★★★"
    }
    ListElement{
        name: "Elephants Dream"
        year: "2015"
        location: "Assets/images/poster_lephantsDream.jpg"
        score: "★★★☆☆"
    }

    ListElement{
        name: "Sita Sing The Blues"
        year: "2016"
        location: "Assets/images/poster_sitaSingTheBlues.jpg"
        score: "★★★☆☆"
    }

    ListElement{
        name: "Start Trek Into Darkness"
        year: "2015"
        location: "Assets/images/poster_startTrekIntoDarkness.jpg"
        score: "★★★☆☆"
    }
}
