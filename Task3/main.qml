import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.XmlListModel 2.0
 Window {
    id: root
    visible: true
    title: qsTr("Task 3")
    visibility: Window.Maximized
    color: "grey"

    GridView {
        id:view
        anchors.fill: parent
        cellWidth: view.width / 5           // 5 elements in a row
        cellHeight: cellWidth * 1.7         // just a generic num looks good :D
        highlightMoveDuration: 200
        focus: true
        delegate: Rectangle {
            id: selector
            width: view.cellWidth           // take predefined cell width (one of 5 rows)
            height: view.cellHeight
            color: 'black'
            Image {
                id: movieImg
                source:  selector.focus ? "https://image.tmdb.org/t/p/w500" + modelData.poster_path
                                        : "https://image.tmdb.org/t/p/w500" + modelData.backdrop_path
                opacity: selector.focus ? 1 : 0.5
                anchors.horizontalCenter: parent.horizontalCenter
                fillMode: Image.Stretch // stretch poster image to fill container dimensions !? - note: would prefer Image.PreserveAspectFit
                anchors.fill: parent
                anchors.bottomMargin: selector.height * 0.1
                MouseArea {
                   anchors.fill: parent
                   onClicked: view.currentIndex = index
                }
            }

            Rectangle{
                color: "black"
                width: parent.width
                height: movieImg.anchors.bottomMargin  // using 10% bottom for black rect
                anchors.bottom: selector.bottom
                Text {
                    width: parent.width * 0.9
                    anchors.centerIn: parent
                    elide: Text.ElideRight
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: "white"
                    font.pixelSize: 30
                    // doing ternary operator for title or selected info (date + vote) eg. -> 2017     7.5/10
                    text: selector.focus ? modelData.release_date.slice(0,4) + "\t\t" + modelData.vote_average + "/10" : modelData.title
                }
            }
        }
    }

    // based on eg. from http://doc.qt.io/qt-5/qtquick-demos-tweetsearch-example.html
    Component.onCompleted: {
        var xhr = new XMLHttpRequest()
        var url = "https://api.themoviedb.org/4/list/1"
        xhr.open("GET", url, true)
        xhr.setRequestHeader('Authorization', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5YjBkOGVlMGQzODdiNjdhYTY0ZjAzZDllODM5MmViMyIsInN1YiI6IjU2MjlmNDBlYzNhMzY4MWI1ZTAwMTkxMyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.UxgW0dUhS62m41KjqEf35RWfpw4ghCbnSmSq4bsB32o');
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                var json = JSON.parse(xhr.responseText.toString())
                view.model = json.results
            }
        }
        xhr.send()
    }

 }
