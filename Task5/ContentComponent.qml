import QtQuick 2.11
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import QtQuick.XmlListModel 2.0
import QtMultimedia 5.9
import QtQuick.Layouts 1.3

FocusScope{
    id: focusId

    Component.onCompleted: {
        focusId.forceActiveFocus()
    }

    GridLayout {
        id: grid
        property bool layoutChanged
        width: root.width
        height: root.height * 0.9
        layoutDirection: Qt.RightToLeft
        // note to myself -> Screen.desktopAvailableHeight > Screen.desktopAvailableWidth is also cool
        flow:  width > height ?  GridLayout.LeftToRight : GridLayout.TopToBottom    // change flow based on width and height
        onFlowChanged: layoutChanged = !layoutChanged
        onFocusChanged: focus ? videoScreen.play() : videoScreen.pause()

        Rectangle {
            id: videoRect
            Layout.minimumHeight: grid.flow === GridLayout.TopToBottom ? parent.height * 0.4: parent.height
            Layout.minimumWidth: grid.flow === GridLayout.TopToBottom ? parent.width : parent.width * 0.6
            color: "black"
            Video
            {
                id: videoScreen
                anchors.fill: parent
                state: 'normal'
                autoPlay: true

                MouseArea
                {
                    anchors.fill: parent
                    onClicked: {
                        controlToogle = true
                        toggleTimer.restart()
                    }
                }

                Rectangle
                {
                    id: control
                    width: parent.width
                    height: videoScreen.height/7
                    color: 'transparent'
                    anchors.centerIn: videoScreen
                    visible: opacity > 0 ? true : false
                    opacity: controlToogle ? 1 : 0

                    Row{
                        width: parent.width
                        height: parent.height
                        leftPadding: parent.width * 0.12
                        spacing: parent.width * 0.3

                        // Refresh icon
                        Icon
                        {
                            source: "android-refresh"
                            onClicked: {
                                videoScreen.stop(); videoScreen.play();
                                controlToogle = true; toggleTimer.restart()
                            }
                        }

                        Icon
                        {
                            width: parent.width * 0.05
                            source: videoScreen.playbackState == MediaPlayer.PlayingState ? "pause" : "play"
                            onClicked: {
                                videoScreen.playbackState == MediaPlayer.PlayingState ? videoScreen.pause() : videoScreen.play();
                                controlToogle = true; toggleTimer.restart()
                            }
                        }

                        // Stop icon
                        Icon
                        {
                            source: "stop"
                            onClicked: {
                                videoScreen.stop();
                                controlToogle = true; toggleTimer.restart()
                            }
                        }
                    }

                    // Channel name
                    Rectangle
                    {
                        id: chanellRect
                        width: parent.width
                        height: videoScreen.height/7
                        color: 'transparent'
                        anchors.top: control.bottom

                        Text
                        {
                            id: channelName
                            font.pixelSize: parent.width * 0.06
                            color: '#fff'
                            anchors.centerIn: parent
                        }
                    }

                    // Timer
                    Rectangle
                    {

                        id: timerRect
                        width: parent.width
                        height: videoScreen.height/7
                        color: 'transparent'
                        anchors.top: chanellRect.bottom

                        // Time start
                        Text{
                            id: startID
                            font.pixelSize: parent.width * 0.06
                            color: '#fff'
                            anchors.leftMargin: parent.width * 0.05
                            anchors.left: parent.left
                            anchors.bottom: loadingBar.top
                        }


                        ProgressBar{
                            id: loadingBar
                            width: parent.width * 0.9
                            height: parent.height * 0.2
                            anchors.bottom: timerRect.bottom
                            anchors.horizontalCenter: parent.horizontalCenter
                            value: 0.0

                            background: Rectangle {
                                id: greyLoading
                                implicitWidth: 200
                                implicitHeight: 6
                                color: "grey"
                                radius: 3
                            }

                            contentItem: Item {
                                implicitWidth: 200
                                implicitHeight: 4

                                Rectangle {
                                    width: loadingBar.visualPosition * parent.width
                                    height: parent.height
                                    radius: 2
                                    color: "red"
                                }
                            }
                        }

                        // Time end
                        Text{
                            id: endID
                            font.pixelSize: parent.width * 0.06
                            color: '#fff'
                            anchors.rightMargin: parent.width * 0.05
                            anchors.right: parent.right
                            anchors.bottom: loadingBar.top
                        }


                        Timer {
                            id: loadingTimer
                            interval: 1000 // triggers every 1 sec
                            onTriggered:
                            {
                                loadingBar.value = videoScreen.position / videoScreen.duration
                            }
                            repeat: true
                            running: true
                        }
                    }
                }

                Timer{
                    id: toggleTimer
                    interval: 5000;
                    running: true
                    onTriggered: controlToogle = false}

                Rectangle
                {
                  id: resize
                  anchors.fill: parent
                  color: 'transparent'
                  visible: opacity > 0 ? true : false
                  opacity: controlToogle ? 1 : 0
                 // color: "green"

                  Icon{
                    anchors.right: parent.right
                    topPadding: parent.height * 0.1
                    rightPadding: parent.width * 0.12
                    source: videoScreen.state == 'normal' ? "arrow-expand" : "arrow-shrink"
                    onClicked:
                    {
                        videoScreen.state = videoScreen.state == 'normal' ? 'fullscreen' : 'normal'
                    }
                  }
                }

                states: [
                    State {
                        name: "fullscreen"
                            PropertyChanges {target: videoRect; Layout.minimumWidth:grid.width; Layout.minimumHeight: grid.height}
                            PropertyChanges {target: channelRect; visible: false}
                    }
                ]
            }
        }
        Rectangle {
            id: channelRect
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "grey"
            ListView{
                id: listView
                anchors.fill: parent
                spacing: 1
                highlightMoveDuration: 200
                clip: true
                model: xmlListModel
                delegate: channelDelegate
                focus: true
                onCurrentItemChanged:
                {
                    videoScreen.source = xmlListModel.get(listView.currentIndex).url
                    channelName.text = xmlListModel.get(listView.currentIndex).uid
                    startID.text = xmlListModel.get(listView.currentIndex).start
                    endID.text = xmlListModel.get(listView.currentIndex).end
                    controlToogle = false
                }
            }
        }

        Component
        {
            id: channelDelegate

            Rectangle{
                id: column
                width: listView.width
                height: listView.height / 5
                color: "grey"

                MouseArea {
                   anchors.fill: parent
                   onClicked: {listView.currentIndex = index
                   }
                }

                // Left part of [column icon] 25%
                Rectangle{
                    id: iconColumn
                    width: parent.width * 0.25
                    height: parent.height
                    color:"black"
                    Image
                    {
                        id: logoImg
                        fillMode: Image.PreserveAspectFit
                        anchors.centerIn: parent
                        source: "Assets/" + icon
                    }
                }

                // Center part of column [channel name] 55%
                Rectangle{
                    id: infoColumn
                    width: parent.width * 0.55
                    height: parent.height
                    color:"black"
                    anchors.left: iconColumn.right
                    anchors.right: pointSelect.left
                    anchors.margins: 1
                    Text{
                       width: parent.width * 0.9
                       elide: Text.ElideRight
                       anchors.verticalCenter: parent.verticalCenter
                       leftPadding: listView.width / 12
                       color: column.focus ? 'red' : 'white'
                       font.pixelSize: 35
                       text: uid
                   }

                   // Need two text component can't get elide to work with multi line text
                   Text{
                       width: parent.width * 0.9
                       elide: Text.ElideRight
                       anchors.verticalCenter: parent.verticalCenter
                       topPadding: listView.height / 10
                       leftPadding: listView.width / 12
                       color: column.focus ? 'red' : 'white'
                       opacity: 0.5
                       font.pixelSize: 25
                       text: start + " - " + end
                   }
               }

                // Right part of column [point select] 20%
                Rectangle{
                    id: pointSelect
                    width: parent.width * 0.2
                    height: parent.height
                    color:"black"
                    anchors.right: parent.right

                    Icon
                    {
                        font.pixelSize: parent.width * 0.6
                        color: "grey"
                        source: "ios-arrow-right"
                        anchors.centerIn: parent
                    }

               }
            }
        }

        XmlListModel
        {
            id: xmlListModel
            source: "Assets/resources/channels.xml"
            query: "/channels/channel"
            XmlRole {name: "uid"; query: "uid/string()"}
            XmlRole {name: "icon"; query: "icon/string()"}
            XmlRole {name: "start"; query: "start/string()"}
            XmlRole {name: "end"; query: "end/string()"}
            XmlRole {name: "url"; query: "url/string()"}
        }

    }
}
