import QtQuick 2.11
import './ionicons.js' as Ionicons

Text
{
    property string source: ""
    signal clicked

    font.pixelSize: parent.width * 0.08
    color: '#fff'
    text: Ionicons.img[source]

    MouseArea
    {
        id: mouseArea
        anchors.fill: parent
        onClicked: parent.clicked()
    }
}
