import QtQuick 2.11
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import QtQuick.XmlListModel 2.0
import QtMultimedia 5.9
import QtQuick.Layouts 1.3

ApplicationWindow {
    id: root
    visible: true
    width: 1920//Screen.width
    height: 1080//Screen.height
    FontLoader { source: "ionicons.ttf"; id:loader }
    property bool controlToogle: true

    Loader
    {
        id: nextPage
        anchors.fill: parent
    }

    header: Row{
            id: headerRect
            width: root.width
            height: root.height * 0.1
            anchors.top: root.top

            Rectangle{
                id: taskTitle
                width: parent.width * 0.4
                height: parent.height
                color: "black"

                Text {
                    id: titleText
                    width: parent.width
                    height: parent.height
                    font.pixelSize: width * 0.15
                    verticalAlignment: Qt.AlignVCenter
                    leftPadding: parent.width * 0.02
                    color: "white"
                    text: "Task 5"
                }
            }

            Rectangle
            {
                id: search
                width: parent.width * 0.4
                height: parent.height
                color: "black"

                Icon
                {
                    font.pixelSize: parent.width * 0.16
                    source: "search"
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    onClicked: {
                        if(stackView.depth === 1)
                            stackView.push("EmptyPage.qml")
                    }
                }
            }

            Rectangle{
                id: back
                width: parent.width * 0.1
                height: parent.height
                color: "black"

                Icon
                {
                    font.pixelSize: parent.width * 0.9
                    source: "reply"
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    anchors.rightMargin: parent.width * 0.1
                    onClicked:  stackView.depth === 2 ? stackView.pop() : Qt.quit()
                }


            }

            Item {
                width: 1 // dummy value != 0
                height: parent.height
            }

            Rectangle{
                id: time
                width: parent.width * 0.1
                height: parent.height
                color: "black"

                Text{
                    id: hours
                    width: parent.width * 0.5
                    height: parent.height
                    font.pixelSize: width
                    leftPadding: parent.width * 0.08
                    color: "white"
                    verticalAlignment: Qt.AlignVCenter
                    text:  Qt.formatDateTime(new Date(), "HH")
                }

                Text{
                    id: minutes
                    width: parent.width * 0.5
                    height: parent.height
                    font.pixelSize: width * 0.5
                    color: "white"
                    verticalAlignment: Qt.AlignVCenter
                    anchors.right: parent.right
                    leftPadding: time.width * 0.15
                    bottomPadding: time.width * 0.2
                    text: Qt.formatDateTime(new Date(), "mm")
                }

                Text{
                    id: underStroke
                    width: parent.width * 0.5
                    height: parent.height
                    font.pixelSize: width * 0.5
                    color: "white"
                    verticalAlignment: Qt.AlignVCenter
                    anchors.right: parent.right
                    leftPadding: time.width * 0.17
                    bottomPadding: time.width * 0.2
                    text: "__"
                }
            }

            Timer {
                id: timerClock
                interval: 60000 // triggers every 1min or 60000 msc
                onTriggered: {
                    hours.text = Qt.formatDateTime(new Date(), "HH")
                    minutes.text =  Qt.formatDateTime(new Date(), "mm")
                }

                repeat: true
                running: true
            }
    }


    // Main stackview
    StackView{
        id: stackView
        anchors.fill: parent
    }

    // After loading show intiial page
    Component.onCompleted: {
        stackView.push("ContentComponent.qml")
    }

    // Logout and show login page
    function returnToDashboard()
    {
        stackView.pop()
    }

}
