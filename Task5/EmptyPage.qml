import QtQuick 2.11
import QtQuick.Controls 2.2
import QtMultimedia 5.9

Rectangle{
    id: closeRect
    color: "grey"

    signal playVideo

    Icon{
        source: "reply"
        anchors.centerIn: closeRect
        onClicked:
        {
            returnToDashboard()
        }
    }
}
