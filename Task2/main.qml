import QtQuick 2.11
import QtQuick.Window 2.2
import QtQuick.XmlListModel 2.0

Window {
    id: root
    visible: true
    width: Screen.width
    height: Screen.height
    title: qsTr("Task2")
    color: "black"

    ListView{
        id: listView
        width: parent.width
        height: parent.height / 1.5             // taking aspect into account
        highlightRangeMode: ListView.ApplyRange // don't scroll out range
        preferredHighlightBegin: 0
        preferredHighlightEnd: listView.width
        highlightMoveDuration: 100
        model: xmlListModel

        orientation: Qt.Horizontal
        anchors.centerIn: parent

        delegate:
        Item{
            id: itemId
            width: root.width<300 ? root.width/3 : root.width/5 // if more then 300 widht display 3 items
            height: ListView.view.height * 0.7

            property bool isCurrentItem: ListView.isCurrentItem
            property int _index: index

            Image
            {
                id: posterImg
                source: poster

                scale: parent.isCurrentItem ? 1 : 0.8
                smooth: mouseArea.containsMouse

                height: parent.height
                width: parent.width

                fillMode: Image.PreserveAspectFit               // perserve aspect

                MouseArea {
                    id: mouseArea
                    anchors.fill: parent
                    hoverEnabled: true

                    onClicked: {
                        listView.currentIndex = itemId._index
                    }
                }

                Behavior on scale {
                   PropertyAnimation {
                       duration: 300
                       easing.type: Easing.InOutQuad
                   }
               }
            }

            Item
            {
                id: posterText
                width: posterImg.width
                height: posterImg.height * 0.3
                anchors.top: posterImg.bottom
                Text{
                    width: parent.width * 0.8
                    anchors.centerIn: parent
                    text: parent.isCurrentItem ? score : name + " [" + year + "]"
                    color: "white"
                    elide: Text.ElideRight
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: 30

                    Behavior on text {
                       PropertyAnimation {
                           duration: 200
                           easing.type: Easing.InOutQuad
                       }
                    }
                }
            }
        }
        focus: true
    }

    XmlListModel{
        id: xmlListModel
        source: "Assets/resources/model.xml"
        query: "/Data/Movie"

        XmlRole{name: "score"; query:"score/string()"}
        XmlRole{name: "name"; query:"name/string()"}
        XmlRole{name: "year"; query:"year/string()"}
        XmlRole{name: "poster"; query:"poster/string()"}
    }
}
