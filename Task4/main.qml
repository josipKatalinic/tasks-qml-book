import QtQuick 2.11
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import QtQuick.XmlListModel 2.0
import QtMultimedia 5.9
import QtQuick.Layouts 1.3

Window {
    id: root
    visible: true
    width: Screen.width
    height: Screen.height

    GridLayout {
        id: grid
        property bool layoutChanged
        anchors.fill: parent
        layoutDirection: Qt.RightToLeft
        // note to myself -> Screen.desktopAvailableHeight > Screen.desktopAvailableWidth is also cool
        flow:  width > height ?  GridLayout.LeftToRight : GridLayout.TopToBottom    // change flow based on width and height
        onFlowChanged: layoutChanged = !layoutChanged
        Rectangle {
            id: videoRect
            property double widthFactor : 0.7       // just in case initializing value
            property double heightFactor : 1

            // logic for scalability
            onWidthChanged:
            {
               //  console.log("We are in portrait mode: " + grid.layoutChanged) // used for debugging layoutModes
                if(grid.layoutChanged)
                {
                    widthFactor = 1
                    heightFactor = 0.4
                }
                else
                {
                    widthFactor = 0.7
                    heightFactor = 1
                }
            }

            Layout.minimumWidth:  root.width * widthFactor
            Layout.minimumHeight: root.height * heightFactor
            color: "black"
            Video
            {
                id: videoScreen
                anchors.fill: parent
                state: 'normal'
                autoPlay: true

                MouseArea
                {
                    anchors.fill: parent
                    hoverEnabled: true
                    onPositionChanged:
                    {
                        control.visible = true
                        timer.running = true
                        opacityAnim.running = true
                    }
                    onExited:
                    {
                        timer.running = true
                        opacityAnim.running = true
                    }
                }


                Image
                {
                    id: control
                    height: 150
                    width: height
                    anchors.centerIn: parent
                    source: videoScreen.playbackState == MediaPlayer.PlayingState ? "Assets/images/pause.png" : "Assets/images/play.png"
                    MouseArea
                    {
                        anchors.fill: parent
                        onClicked:  {videoScreen.playbackState == MediaPlayer.PlayingState ? videoScreen.pause() : videoScreen.play();
                            timer.running = !timer.running; opacityAnim.running = !opacityAnim.running
                        }
                    }

                    scale: visible ? 1.0 : 0.1
                    Behavior on scale {
                      NumberAnimation  { duration: 300 ; easing.type: Easing.Linear  }
                     }

                    OpacityAnimator {
                        id: opacityAnim
                        target: control;
                        from: 1;
                        to: 0;
                        duration: 2000
                        running: false
                    }
                }

                Rectangle
                {
                  id: resize
                  anchors.bottom: parent.bottom
                  width: parent.width
                  color: 'transparent'
                  height: parent.height/7
                  Image
                  {
                      height: parent.height*0.8
                      width: height
                      anchors.right: parent.right
                      anchors.verticalCenter: parent.verticalCenter
                      source: videoScreen.state == 'normal' ? "Assets/images/expand.png" : "Assets/images/contract.png"
                      MouseArea
                      {
                          anchors.fill: parent
                          onClicked:
                          {
                              if (MediaPlayer.PlayingState)
                              {
                                  videoScreen.state = videoScreen.state == 'normal' ? 'fullscreen' : 'normal'
                              }
                          }
                      }
                  }
                }

                states: [
                    State {
                        name: "fullscreen"
                            PropertyChanges {target: videoRect; Layout.minimumWidth:root.width; Layout.minimumHeight: root.height}
                            PropertyChanges {target: channelRect; visible: false}
                    }
                ]

                Timer {
                    id: timer
                    interval: 2000 // triggers every 2000 ms
                    onTriggered: control.visible = false
                    running: false
                }


                Component.onCompleted:
                {
                    timer.running = true // default startup hide after 5 sec
                }
            }
        }
        Rectangle {
            id: channelRect
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "#1e1b18"
            ListView{
                id: listView
                anchors.fill: parent
                spacing: (parent.width / parent.height) * 5
                highlightMoveDuration: 200
                clip: true

                onCurrentItemChanged:
                {
                   videoScreen.source = xmlListModel.get(listView.currentIndex).url
                }


                model: xmlListModel
                delegate: channelDelegate

                focus: true
            }
        }
    }

    Component
    {
        id: channelDelegate
        Rectangle{
            id: column
            width: listView.width
            height: listView.height / 5
            color: "black"

            MouseArea {
               anchors.fill: parent
               onClicked: listView.currentIndex = index
            }

            Text{
               leftPadding: listView.width / 12
               anchors.verticalCenter: column.verticalCenter
               color: column.focus ? 'red' : 'white'
               font.pixelSize: Math.round(listView.height / 20)
               text: uid
           }
        }
    }

    XmlListModel
    {
        id: xmlListModel
        source: "Assets/resources/channels.xml"
        query: "/channels/channel"
        XmlRole {name: "uid"; query: "uid/string()"}
        XmlRole {name: "url"; query: "url/string()"}
    }
}
